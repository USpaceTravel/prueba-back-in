package com.prueba.credibanco.backinc.service;

import com.prueba.credibanco.backinc.repository.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.smartcardio.Card;

@Service
public class CardService {

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    public Card generateCard(String productId, String cardHolderName) {
        String cardNumber = generateCardNumber(productId);
        Card card = new Card();
        card.setCardNumber(cardNumber);
        card.setCardHolderName(cardHolderName);
        card.setBalance(0);
        card.setActive(false);

        return cardRepository.save(card);
    }

    public String generateCardNumber(String productId) {
        // Lógica para generar el número de tarjeta a partir del productId
        // Ejemplo simplificado
        String cardNumber = productId + "1234567890";
        return cardNumber;
    }

    public boolean activateCard(String cardId) {
        Card card = cardRepository.findById(cardId).orElse(null);
        if (card != null) {
            card.setActive(true);
            cardRepository.save(card);
            return true;
        }
        return false;
    }

    public boolean rechargeCardBalance(String cardId, double amount) {
        Card card = cardRepository.findById(cardId).orElse(null);
        if (card != null) {
            double currentBalance = card.getBalance();
            card.setBalance(currentBalance + amount);
            cardRepository.save(card);
            // Registra la transacción de recarga
            Transaction transaction = new Transaction();
            transaction.setCard(card);
            transaction.setAmount(amount);
            transaction.setTransactionType(TransactionType.RECHARGE);
            transactionRepository.save(transaction);
            return true;
        }
        return false;
    }
}
