package com.prueba.credibanco.backinc.repository;


import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.smartcardio.Card;

@Repository
public interface CardRepository extends JpaRepository<Card, Long> {

}
