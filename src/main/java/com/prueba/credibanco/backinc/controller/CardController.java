package com.prueba.credibanco.backinc.controller;

import com.prueba.credibanco.backinc.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/card")
public class CardController {

    @Autowired
    private CardService cardService;

    @GetMapping("/{productId}/number")
    public String generateCardNumber(@PathVariable String productId) {
        return cardService.generateCardNumber(productId);
    }

    @PostMapping("/enroll")
    public ResponseEntity<String> activateCard(@RequestBody String cardId) {
        boolean success = cardService.activateCard(cardId);
        if (success) {
            return ResponseEntity.ok("Card activated successfully.");
        } else {
            return ResponseEntity.badRequest().body("Card activation failed.");
        }
    }

}
