package com.prueba.credibanco.backinc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private double amount;
    private TransactionType transactionType;

    @ManyToOne
    private Card card;

    // Getters y setters

    // Otros campos y métodos según tus necesidades
}
