package com.prueba.credibanco.backinc.model;

public enum TransactionType {
    PURCHASE("Purchase"),
    RECHARGE("Recharge"),
    ANNUAL_FEE("Annual Fee"),
    // Agrega otros tipos de transacciones según tus necesidades

    private final String displayName;

    TransactionType(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
    }
