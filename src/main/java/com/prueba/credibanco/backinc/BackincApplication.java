package com.prueba.credibanco.backinc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackincApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackincApplication.class, args);
	}

}
